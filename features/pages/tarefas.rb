class Tarefas < SitePrism::Page

   set_url 'https://qa-test.avenuecode.com/tasks'
   element :botao_myTasks, 'input[class="btn-lg"]'
   element :botao_add_tarefa, 'input[class="input-group-addon"]'
   element :campo_nova_tarefa, '#new_task'
   element :botao_remover_tarefa, 'button[class="btn-danger"]'
   element :campo_add_subtarefa, '#new_sub_task'
   element :campo_data, '#dueDate'
   element :botao_add_subtarefa, '#add-subtask'


    def deve_cadastrar_tarefa
        campo_nova_tarefa.set 'Adicionando uma nova tarefa'
        page.find(:css, 'span[ng-click="addTask()"]').click
    end

    def deve_remover_tarefa
        page.all(:css, 'button[ng-click="removeTask(task)"]')[0].click
    end

   def deve_cadastrar_subtarefa
        page.all(:css, 'button[ng-click="editModal(task)"]')[0].click
        campo_add_subtarefa.set  'Adicionando uma nova Sub Tarefa'
        campo_data.set '08/09/2019'
        botao_add_subtarefa.click
    end
end 