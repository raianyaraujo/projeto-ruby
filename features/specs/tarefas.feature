#language:pt
Funcionalidade: Cadastra Tarefa
Eu como usuario quero cadastrar uma tarefa

    Contexto: Deve Realizar Login
        Dado que eu faco login
        
    @1
    Cenario: Deve cadastrar uma tarefa

        Quando eu adiciono uma nova tarefa
        Entao a tarefa deverá ser inserida no TodoList

    @2
    Cenario: Deve remover uma tarefa

        Quando eu remover uma tarefa
        Entao a tarefa nao será mais apresentada no TodoList

    @3
    Cenario: Deve adicionar uma subtarefa  

            Quando eu adicionar uma subtarefa
            Entao a tarefa deverá ser inserida no Todo de subtarefas