Dado("que eu faco login") do
  @home = HomePage.new
  @home.load
  @home.deve_realizar_login
end

Quando("eu adiciono uma nova tarefa") do 
  @tarefas = Tarefas.new
  @tarefas.load 
  @tarefas.deve_cadastrar_tarefa
  expect(page).to have_content "Raiany de Araujo Silva's ToDo List"
end


Entao("a tarefa deverá ser inserida no TodoList") do
  expect(page).to have_content "Adicionando uma nova tarefa"
end

Quando ("eu remover uma tarefa") do
@tarefas = Tarefas.new
@tarefas.load
@tarefas.deve_remover_tarefa
end

Entao ("a tarefa nao será mais apresentada no TodoList") do
page.should have_no_content "Adicionando uma nova tarefa"
end

Quando ("eu adicionar uma subtarefa") do
  @tarefas = Tarefas.new
  @tarefas.load 
  @tarefas.deve_cadastrar_tarefa
  @tarefas.deve_cadastrar_subtarefa
  end

  Entao("a tarefa deverá ser inserida no Todo de subtarefas") do
    expect(page).to have_content "Adicionando uma nova Sub Tarefa"
  end