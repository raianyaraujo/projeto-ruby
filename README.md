# Documentação 

  - [Documento com observações referente ao teste automático](https://bitbucket.org/raianyaraujo/projeto-ruby/src/master/Documenta%C3%A7%C3%A3o/Documento%20com%20observa%C3%A7%C3%B5es%20referente%20ao%20teste%20automatico.pdf)
  - [Documento com as evidencias dos bugs encontrados, demarcados com os Ids dos bugs.](https://bitbucket.org/raianyaraujo/projeto-ruby/src/master/Documenta%C3%A7%C3%A3o/Documento%20de%20Evid%C3%AAncias.pdf)
  - [Documento com as estratégias de testes e considerações](https://bitbucket.org/raianyaraujo/projeto-ruby/src/master/Documenta%C3%A7%C3%A3o/Estrat%C3%A9gias%20de%20teste.pdf)
  - [Documento com o passo a passo da instalação do Ruby ](https://bitbucket.org/raianyaraujo/projeto-ruby/src/master/Documenta%C3%A7%C3%A3o/Instala%C3%A7%C3%A3o%20do%20Ruby.pdf)

